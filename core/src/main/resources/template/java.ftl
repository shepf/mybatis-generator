package ${packageModel};

import lombok.Getter;
import lombok.Setter;
import lombok.ToString;

import java.io.Serializable;

/**
*
*  @author ${author}
*/
@Getter
@Setter
@ToString
public class ${className} implements Serializable {

    private static final long serialVersionUID = ${timeStamp}L;

<#list attrs as attr>

    <#if attr.remarks!="" || attr.nullAble?? ||attr.columnDef??>
    /**
    <#if attr.isKey == 1>
    * 主键
    </#if>
    * ${attr.remarks}
    */
    </#if>
    private ${attr.javaTypeName} ${attr.propertiesName};
</#list>


}
