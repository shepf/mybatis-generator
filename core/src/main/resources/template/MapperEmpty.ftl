package ${packageMapper};

import ${packageModel}.${className};
import tk.mybatis.mapper.common.Mapper;


/**
*  @author ${author}
*/
public interface ${mapperName}Mapper extends Mapper<${className}>{


}