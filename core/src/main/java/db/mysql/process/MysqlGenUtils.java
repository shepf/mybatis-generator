package db.mysql.process;

import db.mysql.env.Constants;
import db.mysql.env.RuntimeEnv;
import db.mysql.env.TemplateEnum;
import db.mysql.model.GenrateParamReq;
import freemarker.template.Configuration;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import freemarker.template.TemplateExceptionHandler;

import javax.swing.*;
import java.io.*;
import java.util.Map;

/**
 * db.mysql
 * 具体的生成模板文件的工具类
 */
public class MysqlGenUtils {

    public static void genrate(Map<String, Object> root) throws IOException, TemplateException {
        gen(GenrateParamReq.GenrateParamReqBuilder.aGenrateParamReq()
                .withOutPath(RuntimeEnv.pp.getModelOutPath())
                .withFileName(RuntimeEnv.pp.getClassName() + Constants.Java_Type_Suffix)
                .withTemplateName(TemplateEnum.Java.getText())
                .withOverwrite(true)
                .withTemplateParam(root)
                .build());
        //是否读写分离
        if (!RuntimeEnv.pp.isSperateRead()) {

            //参数使用 GenrateParamReq 实例封装
            gen(GenrateParamReq.GenrateParamReqBuilder.aGenrateParamReq()
                    .withOutPath(RuntimeEnv.pp.getMapperOutPath())
                    .withFileName(RuntimeEnv.pp.getMapperName() + Constants.Mapper_Suffix + Constants.Java_Type_Suffix)
                    .withTemplateName(TemplateEnum.MapperEmpty.getText())
                    .withTemplateParam(root)
                    .withOverwrite(false)
                    .build());

            gen(GenrateParamReq.GenrateParamReqBuilder.aGenrateParamReq()
                    .withOutPath(RuntimeEnv.pp.getMapperXmlOutPath())
                    .withFileName(RuntimeEnv.pp.getMapperXmlName() + Constants.Mapper_Suffix + Constants.Xml_Type_Suffix)
                    .withTemplateName(TemplateEnum.MapperXmlEmpty.getText())
                    .withOverwrite(false)
                    .withTemplateParam(root)
                    .build());

        }
    }

    @SuppressWarnings("ResultOfMethodCallIgnored")
    private static void gen(GenrateParamReq req) throws IOException, TemplateException {
        Configuration cfg = new Configuration(Configuration.VERSION_2_3_23);
        cfg.setClassForTemplateLoading(MysqlGenUtils.class, "/template");
        cfg.setDefaultEncoding("UTF-8");
        cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        Template temp = cfg.getTemplate(req.getTemplateName());

        // Create the root hash

        File dir = new File(req.getOutPath());
        if (!dir.exists()) {
            dir.mkdirs();
        }
        File file = new File(dir, req.getFileName());
        int isProduce;
        if (file.exists() && req.isOverwrite()) {
            isProduce = JOptionPane.showConfirmDialog(null, req.getFileName() + "文件已存在，是否生成", "提示", JOptionPane.YES_NO_OPTION);
        } else if (file.exists() && !req.isOverwrite()) {
            isProduce = 1;
        } else {
            isProduce = 0;
        }
        if (isProduce == 0) {
            OutputStream fos = new FileOutputStream(new File(dir, req.getFileName())); //文件的生成目录
            Writer out = new OutputStreamWriter(fos, "UTF-8");
            temp.process(req.getTemplateParam(), out);
            fos.flush();
            fos.close();
            System.out.println(req.getFileName() + "gen code success!");
        }
    }
}
