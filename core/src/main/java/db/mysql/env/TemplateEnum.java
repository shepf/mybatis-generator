package db.mysql.env;

/**
 * db.mysql.env
 * 定义了目标文件名
 */
public enum  TemplateEnum {
    Java("java.ftl"),
    MapperEmpty("MapperEmpty.ftl"),
    MapperXmlEmpty("MapperXmlEmpty.ftl")
    ;

    private String text;

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    TemplateEnum(String text) {
        this.text = text;
    }

    @Override
    public String toString() {
        return "TemplateEnum{" +
                "text='" + text + '\'' +
                '}';
    }
}
